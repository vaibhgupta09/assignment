/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,TouchableOpacity,
   ActivityIndicator,
  View,Dimensions,Alert
} from 'react-native';
import Camera from 'react-native-camera';

let width= Dimensions.get('window').width;
let height= Dimensions.get('window').height;
export default class App extends Component<{}> {
 constructor(props) {
    super(props);

    this.state = {
      scanning:true,
      latitude: null,
      longitude: null,
      error:null,
    };
  }
  componentdidMount() {
    console.log("componentdidMount Called");
    
    navigator.geolocation.getCurrentPosition(
       (position) => {
         
         console.log(this.state.scanning);
         this.setState({
         
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           
           error: null,
         });
       },
       (error) => this.setState({ error: error.message }),
       { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
     );

   }
  scanStart()
  {
    this.setState({
           scanning:true,
           });
  }

scanLoop(e)
{
if(this.state.scanning)
{
  Alert.alert(
  'Data-',
  ''+e.data,
  [
    {text: 'OK', onPress: () => this.scanStart()},
  ],
  { cancelable: false }
)
   this.setState({
           scanning:false,
           });

}

}
onBarCodeRead = (e) => this.scanLoop(e);



  render() {

    return (
      <View style={styles.container}>
        <Text>Current latitude and longitude</Text>
        <Text>latitude- {this.state.latitude} </Text>
        <Text>longitude- {this.state.longitude} </Text>
        <Text> {this.state.error} </Text>
        <View>
        <TouchableOpacity onPress={this.componentdidMount()}>
        <Text style={{color:'black',fontSize:16}}>Refresh</Text>
        </TouchableOpacity>
        </View>
         <Camera
                style={{width:width/1.5,height:height/3}}
                onBarCodeRead={this.onBarCodeRead}
                    ref={cam => this.camera = cam}
                aspect={Camera.constants.Aspect.fill}
                ></Camera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
   preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
